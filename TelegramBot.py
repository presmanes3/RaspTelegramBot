#!/usr/bin/python3
# -*- coding: utf-8 -*-

#--- IMPORTS ---#
import os, time

#--- FROMS ---#
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from tempfile import NamedTemporaryFile
from SpeechRecognition import SR


updater = Updater(token='TOKEN')
dispatcher = updater.dispatcher

def start(bot, update):
	bot.send_message(chat_id=update.message.chat_id, text="Hi, my name is Presmanes_Bot. How could I help You?")
def get_aud(bot, update):
	update.message.reply_text( ">>> Audio Received")
def get_doc(bot, update):
        update.message.reply_text(">>> Document Received")
def get_ima(bot, update):
        update.message.reply_text(">>> Image Received")
def get_vid(bot, update):
        update.message.reply_text(">>> Video Received")
def get_voice(bot, update):
	time_ = time.time()
	#Get The File
	voiceID = update.message.voice.file_id
	file = bot.getFile(voiceID)

	#Download the file
	file.download("Voice.ogg")
	
	#Another command
	"""Hz = #this could be 8000 / 16000 / 32000 / 44100 / 48000 ... Bigger, more audio quality
	command_sample_variation = "ffmpeg -y -sample_fmt s16 -i Voice.ogg -ar %s Voice.wav" %Hz """
	
	#This bash script just transform the '.ogg' audio file from TG to a '.wav' that needs the sr
	command = "ffmpeg -y -sample_fmt s16 -i Voice.ogg Voice.wav -threads 0"

	#Convert file.ogg to file.wav
        wav_time = time.time()
	os.system(command)
	wav_time = time.time() - wav_time

	#Create instance from SpeechRecognition
	spch = SR()
	spch.set_audio_file( os.path.join( os.path.dirname( os.path.realpath(__file__)), "Voice.wav"))
	
	#Message from google
	google_time = time.time()
	message = spch.get_text_from_audio()
	google_time = time.time() - google_time

	#Reply Voice with Text
	total_time = time.time() - time_
	update.message.reply_text("Voice Received\n\nI'm sorry, I know that I've spent : " + str(total_time) + "s\n\n" "But... You said :\n\n" + message, 
					reply_to_message_id=voiceID )
	
	#This script just delete the Voice.wav and Voice.ogg ( you can use the library 'tempfile' with 'NamedTemporaryFile' but i use this for some tests
	os.system("sh reset.sh")
	os.system("sh backup.sh")
        
	#Terminal
        print("\n\n\n---------------")
        print("---------------")
        print("You have said : " + message)
        print("---------------")
        print("---------------\n")
	print(">>> Time to create the '.wav' file : " + str(wav_time) + "\n")
	print(">>> Time to send/receive information from  google : " + str(google_time) + "\n")
	print(">>> Time 'wav' + 'google' : " + str(wav_time + google_time) + "\n")
	print(">>> Total function time : " + str(total_time))
	print(">>> Finished")

#--------------- MAIN PROGRAM ------------------------------#
#Capture Commands		
dispatcher.add_handler(CommandHandler('start', start))

#Capture Voice
dispatcher.add_handler(MessageHandler(Filters.voice, get_voice))

#Capture Audios
dispatcher.add_handler(MessageHandler(Filters.audio, get_aud))

#Capture Documents
dispatcher.add_handler(MessageHandler(Filters.document, get_doc))

#Capture Images
dispatcher.add_handler(MessageHandler(Filters.photo, get_ima))

#Capture Videos
dispatcher.add_handler(MessageHandler(Filters.video, get_vid))

#With clean=True we ignore the files on queu, just take news
updater.start_polling(clean=True)
print(">>> Starting bot.....")
#Wait
updater.idle()

