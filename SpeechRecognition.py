#!/usr/bin/env python3

""" Speech_recognition library and more information about the library here : 'https://pypi.python.org/pypi/SpeechRecognition/' """
#--- IMPORTS ---#
import speech_recognition as sr
import time, os

#--- FROMS ---#
from os import path

class SR(object):
	def __init__(self):
		self.audio_file = None
		
		#Speech Recognition class instance
		self.r = sr.Recognizer()
		self.audio = None

	#Setter
	def set_audio_file(self, audio_file):
		self.audio_file = audio_file
	
	#Setter
	def set_audio(self):
		with sr.AudioFile(self.audio_file) as source:
                        self.audio = self.r.record(source)  # read the entire audio file
	
	#Main function
	def get_text_from_audio(self):
		self.set_audio()
		text = None 
		try:
			text = self.r.recognize_google(self.audio, language="es_ES")
			
		except self.get_UVE():
			text = "Google Speech Recognition could not understand audio"
		
		except self.get_RE():
			text = "Could not request results from Google Speech Recognition service; {0}".format(self.get_RE())
		return text

	#Errors
	def get_UVE(self):
		return sr.UnknownValueError
	def get_RE(self):
		return sr.RequestError 


""" -------------- INGNORE THIS PART, IS JUST FOR TESTING ---------------------------#	
speech = SR()
AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "Voice.wav")
speech.set_audio_file(AUDIO_FILE)
print(">>> Starting....")
time_ = time.time()
print(speech.get_text_from_audio())
print(">>> Time : " + str(time.time() - time_)) 

"""





"""-------------------- ORIGINAL FILE ------------------------------------------------------------------#

AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "Voice.wav")
# AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "french.aiff")
# AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "chinese.flac")

# use the audio file as the audio source
r = sr.Recognizer()
with sr.AudioFile(AUDIO_FILE) as source:
    audio = r.record(source)  # read the entire audio file

try:
    # for testing purposes, we're just using the default API key
    # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
    # instead of `r.recognize_google(audio)`
    time_ = time.time()
    print("Google Speech Recognition thinks you said " + r.recognize_google(audio, language="es_ES"))
    segundos = time.time() - time_
    minutos = segundos / 60
    print("Ha tardado : " + str(minutos) + " minutos // " + str(segundos) + " segundos." )

    #os.system("sh reset.sh")
    
    print("Reset Done!")

except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")

except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))

-------------------- ARCHIVO ORIGINAL ------------------------------------------------------------------ """
